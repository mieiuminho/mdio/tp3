#==============================================================================
SHELL   = zsh
TEX     = pdflatex
#------------------------------------------------------------------------------
FILTERS = -F pandoc-crossref -F pandoc-include-code
OPTIONS = --pdf-engine=$(TEX) --template=styles/template.tex $(FILTERS)
CONFIG  = --metadata-file config.yml
BIB     = --filter pandoc-citeproc --bibliography=references.bib
#------------------------------------------------------------------------------
SRC     = $(shell ls $(SRC_DIR)/**/*.md)
SRC_DIR = sections
REPORT  = report
#------------------------------------------------------------------------------
DAT_DIR = models
OUT_DIR = out
MODELS  = model_2
#==============================================================================

pdf:
	pandoc $(CONFIG) $(OPTIONS) $(BIB) -s $(SRC) -o $(REPORT).pdf

run: setup
	@for MODEL in $(MODELS) ; do \
		lp_solve $(DAT_DIR)/"$$MODEL".lp > $(OUT_DIR)/"$$MODEL".out; \
	done

setup:
	@mkdir -p $(OUT_DIR)

clean:
	@echo "Cleaning..."
	@-cat .art/maid.ascii
	@rm $(REPORT).pdf
	@echo "...✓ done!"

