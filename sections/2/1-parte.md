\cleardoublepage

\part{}

Na variante em análise, cada actividade tem cinco parâmetros adicionais: o
primeiro é o valor do custo normal, expresso em unidades monetárias [U.M.], o
segundo é o valor de $c_1$, o custo suplementar de reduzir a duração da
actividade de uma unidade de tempo [U.T.], expresso em [U.M./U.T.], o terceiro é
o valor da máxima redução de tempo a um custo $c_1$, o quarto é o valor de $c_2$,
o custo suplementar de reduzir a duração da actividade de uma unidade de tempo
[U.T.] após ter aplicado a máxima redução a um custo $c_1$ , expresso em
[U.M./U.T.], e o quinto é o valor da máxima redução de tempo a um custo $c_2$.

| Actividade | Custo Normal | $c_1$ | Máx. red. a custo $c_1$ | $c_2$  | Máx. red. a custo $c_2$ |
|:----------:|:------------:|:-----:|:-----------------------:|:------:|:-----------------------:|
| 0          | 400          | 200   | 0,5                     | 100    | 0,5                     |
| 1          | 1000         | 600   | 1                       | 300    | 1                       |
| 2          | 1400         | 1000  | 3                       | 500    | 1                       |
| 3          | 300          | 200   | 0,5                     | 100    | 0,5                     |
| 4          | 2000         | 800   | 2                       | 400    | 1                       |
| 5          | 1000         | 1600  | 0,5                     | 800    | 0,5                     |
| 6          | 800          | 180   | 1                       | 90     | 1                       |
| 8          | 600          | 200   | 0,5                     | 100    | 0,5                     |
| 10         | 1600         | 1000  | 0,5                     | 500    | 0,5                     |
| 11         | 1400         | 600   | 1                       | 300    | 1                       |

Pretende-se que o tempo de execução do projecto encontrado na PARTE 0 seja
reduzido em 3 U.T.. O objectivo do problema é decidir como devem ser reduzidas
as durações das actividades, de modo a realizar o projecto na nova duração
desejada, com um custo suplementar mínimo.

| Actividade | Duração | Precedências|
|:----------:|:-------:|:-----------:|
| 0          | 4       | -           |
| 1          | 6       | 0           |
| 2          | 7       | 1, 4        |
| 3          | 2       | 2, 5        |
| 4          | 9       | 0, 6        |
| 5          | 4       | 4, 8        |
| 6          | 5       | -           |
| 8          | 4       | 6, 10       |
| 10         | 8       | 6           |
| 11         | 7       | 10          |

Embora  haja  uma  duração  definida  para  cada  actividade,  é  muitas  vezes
possível,  aumentando os recursos nela aplicados, reduzir a sua duração. Isto é
feito com custos suplementares, mas pode trazer o benefício de reduzir a duração
de execução do projecto global.

# Modelação { #sec:model_2 }

O objectivo do problema é decidir como devem ser reduzidas as durações das
actividades, de modo a realizar o projecto em menos 3 [U.T.], com um
custo suplementar mínimo. A técnica utilizada é conhecida como _crashing times_.

O modelo utiliza varáveis de decisão na forma $r_i\_c_1$ e $r_i\_c_2$ e as
variáveis binárias da forma $y_i$.

$r_i\_c_1$: reduções na atividade $i$ ao custo $c_1$

$r_i\_c_2$: reduções na atividade $i$ ao custo $c_2$

$$
y_i =
\begin{cases}
   \text{1, se redução a custo } c_1 \text{ máxima for atingida}\\
   \text{0, caso contrário} \\
\end{cases}
$$

A função objectivo é a minimização da soma dos custos das reduções escolhidas.

$min$ $z:$ $200 r_0\_c_1 + 600 r_1\_c_1 + 1000 r_2\_c_1 + 200 r_3\_c_1
                        + 800 r_4\_c_1+ 1600 r_5\_c_1 + 180 r_6\_c_1
                        + 200 r_8\_c_1 + 1000 r_{10}\_c_1+ 600 r_{11}\_c_1
                        + 100 r_0\_c_2 + 300 r_1\_c_2 + 500 r_2\_c_2
                        + 100 r_3\_c_2+ 400 r_4\_c_2 + 800 r_5\_c_2
                        + 90 r_6\_c_2 + 100 r_8\_c_2 + 500 r_{10}\_c_2
                        + 300 r_{11}\_c_2$

Este modelo exige que o tempo final ($tf$) seja igual ou inferior a 20 de forma
a forçar a redução em 3 unidades ($23 - 3 = 20$).

São adicionas as restrições para cada $r_i\_c_1$ como tendo de ser igual ou
inferior à máxima redução a custo $c_1$ possível. Assim como, para $r_i\_c_2$ a
restrição de ser menor ou igual à redução máxima a custo $c_2$ multiplicando
pela variável binária $y_i$ que indica se a redução ao custo $c_1$ máxima foi ou
não atingida. Esta restrição obriga a $r_i\_c_2$ tomar o valor zero em caso
negativo.

A questão da variável binária tomar o valor 1 quando a redução a custo $c_1$
máxima for atingida pode ser definida através da multiplicação do maior valor
possível por $y_i$ e forçar a ser sempre menor ou igual à redução aplicada.

