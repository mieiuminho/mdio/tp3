# Verificação da Solução { #sec:verificacao }

As reduções das atividades a custo $c_1$ são as seguintes:

$r_0 = 0.5$

$r_3 = 0.5$

$r_6 = 1$

As reduções das atividades a custo $c_2$ são as seguintes:

$r_0 = 0.5$

$r_3 = 0.5$

$r_6 = 1$

As reduções apresentadas geram um tempo final de 20 e têm um custo associado de
570 [U.M.].

A solução é válida uma vez que respeita as restrições apresentadas na
@sec:model_2.

