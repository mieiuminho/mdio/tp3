\part{}

O CPM assume que qualquer actividade pode ser realizada em paralelo com qualquer
outra. É, por exemplo, o caso das actividades 0 e 6. Há, no entanto, situações
em que os recursos são limitados, e a realização das actividades depende dos
recursos disponíveis. Se existir apenas um equipamento utilizado pelas
actividades 0 e 6, as duas actividades não podem decorrer em simultâneo, devendo
a actividade 0 terminar antes de se iniciar a actividade 6 ou a actividade 6
terminar antes de se iniciar a actividade 0. Claramente, pode haver casos em que
a duração global do projecto aumenta em consequência disso.

Para este trabalho, identifique 3 actividades que, no diagrama de Gantt da PARTE
0, decorrem em paralelo; uma das actividades que seleccionar deve pertencer ao
caminho crítico. Considere agora que existe apenas um equipamento para realizar
as três actividades. O objectivo continua a ser realizar o projecto na menor
duração possível.

**1. Explique a forma que escolheu para formular este problema. Identifique
claramente o significado das **novas** restrições e da função objectivo do novo
modelo de programação linear inteira mista. Teça todos os comentários que
considere adequados.**

O objetivo do problema é minimizar o tempo total que custa a execução de
todas as atividades. É de notar que com a remoção dos dois vértices altera o
caminho crítico pelo que podemos usar o seguinte modelo para determinar o
novo caminho crítico.

```{.lp include=lp/critical_path.lp startLine=1 endLine=35}
```

O output obtido após correr o modelo acima é:

```{.lp include=lp/crit_path_output.lp startLine=1 endLine=22}
```

Desta forma conseguimos identificar o novo caminho crítico: 6, 10, 8, 5 e 3.

Podemos agora identificar no diagrama as três atividades que decorrem
paralelamente, através da análise do Diagrama de Gantt, que são, no caso
vertente, as atividades: 1, 4 e 10.

Para a nova versão do problema consideramos, conforme sugerido, que as
atividades referidas não devem ser realizadas simultaneamente uma vez que existe
apenas um equipamento disponível para realizar todas as atividades. A única
maneira de garantir a resolução correta do problema é forçar a execução
sequencial das atividades referidas. Surge agora uma nova situação: devemos
estabelecer uma ordem de execução para as atividades (sendo que temos 6
possibilidades, conforme passamos a mostrar):

* 1 $\rightarrow$ 4 $\rightarrow$ 10
* 1 $\rightarrow$ 10 $\rightarrow$ 4
* 4 $\rightarrow$ 1 $\rightarrow$ 10
* 4 $\rightarrow$ 10 $\rightarrow$ 1
* 10 $\rightarrow$ 1 $\rightarrow$ 4
* 10 $\rightarrow$ 4 $\rightarrow$ 1


Nesta senda, necessitamos apenas de três variáveis binárias para modelar as
diferentes possibilidades de arranjar as combinações. Consideremos a variável
$y_{1,4}$: se esta variável tomar o valor 0 temos que 4 sudece 1 pelo que,
combinando com o valor das restantes variáveis configura um arranjo único
para as atividades.

Desta forma fica claro que necessitamos apenas de 3 três variáveis binárias:

$y_{10,1}$, $y_{10,4}$ e $y_{1,4}$


Podemos estabelecer as seguintes restrições de programação mista por forma a
garantir a execução sequencial das tarefas 2, 5 e 11:

* $t_{1} + 6 <= t_{4} + M - M * y_{1,4}$
* $t_{4} + 9 <= t_{1} + M * y_{1,4}$
* $t_{10} + 8 <= t_{4} + M - M * y_{10,4}$
* $t_{4} + 9 <= t_{10} + M * y_{10,4}$
* $t_{10} + 8 <= t_{1} + M - M * y_{10,1}$
* $t_{1} + 6 <= t_{10} + M * y_{10,1}$


**2. Apresente o ficheiro de input (_cut-and-paste_).**


```{.lp include=lp/input.lp startLine=1 endLine=43}
```

**3. Apresente o ficheiro de output produzido pelo programa (_cut-and-paste_).**


```{.lp include=lp/output.lp startLine=1 endLine=17}
```

**4. Apresente o plano de execução (diagrama de Gantt) do projecto. Verifique
sumariamente que o plano obedece às novas restrições.**


De acordo com o _output_ ficamos a saber que a ordem de execução das atividades sequenciais 1, 4 e 10 deve ser: 1 $\rightarrow$ 4 $\rightarrow$ 10 (`y14 = 1`, `y104 = 0` e `y101 = 0`).

Podemos, então, atualizar o diagrama de Gantt de modo a que se refilta esta alteração:

![Diagrama de Gantt com as atividades 1, 4 e 10 sequenciais](figures/gantt_updated.png){#fig:gantt_updated}

Como todas as atividades são realizadas, se verificam as restrições de fluxo e
as atividades mencionadas acima são realizadas sequencialmente o plano obtido
respeita as restrições.


**5. Faça upload no BlackBoard do ficheiro .lp juntamente com os ficheiros .lp
das outras Partes.**

