# Introdução {#sec:intro}

Devemos começar por referir que o maior número de aluno do nosso grupo de
trabalho é o do aluno Hugo Filipe Duarte Carvalho (A85579).

Assim, de acordo com as indicações presentes no enunciado eliminamos da rede que
representa o projeto as actividades 7 e 9.

![Grafo sem as atividades indicadas](figures/fixed_graph.png){ height=300px #fig:graph }


| Actividade | Duração | Precedências|
|:----------:|:-------:|:-----------:|
| 0          | 4       | -           |
| 1          | 6       | 0           |
| 2          | 7       | 1, 4        |
| 3          | 2       | 2, 5        |
| 4          | 9       | 0, 6        |
| 5          | 4       | 4, 8        |
| 6          | 5       | -           |
| 8          | 4       | 6, 10       |
| 10         | 8       | 6           |
| 11         | 7       | 10          |


Deste modo ficamos com os seguintes arcos associados aos seus respetivos custos, apresentados na forma _Origem $\rightarrow$ Destino_.

* ini $\rightarrow$ 0
* ini $\rightarrow$ 6
* 0 $\rightarrow$ 1
* 0 $\rightarrow$ 4
* 1 $\rightarrow$ 2
* 2 $\rightarrow$ 3
* 3 $\rightarrow$ fim
* 4 $\rightarrow$ 2
* 4 $\rightarrow$ 5
* 5 $\rightarrow$ 3
* 5 $\rightarrow$ fim
* 6 $\rightarrow$ 4
* 6 $\rightarrow$ 8
* 8 $\rightarrow$ 5
* 8 $\rightarrow$ fim
* 10 $\rightarrow$ 11
* 10 $\rightarrow$ 8
* 11 $\rightarrow$ fim


![Diagrama de Gantt](figures/gantt_diagram.png){#fig:gantt}

Conseguimos constatar, pelo diagrama, que a duração do projeto é 23 unidades
temporais.

