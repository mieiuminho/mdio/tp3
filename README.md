# Modelos Determinísticos de Investigação Operacional
> Critical Path Method

O método do caminho crítico, designado na literatura anglo–saxónica por critical
path method (CPM), constitui uma ferramenta muito importante em gestão de
projectos. O método do caminho crítico é aplicado a projectos que podem ser
decompostos num conjunto de actividades, que se considera terem durações
determinísticas, entre as quais existem relações de precedência. As restrições
de precedência traduzem o facto de o instante em que se pode dar início a uma
dada actividade ter de ser posterior aos instantes em que terminam as
actividades que lhe são precedentes.

No método do caminho crítico, a rede que representa o projecto pode ser
representada de duas formas alternativas: uma, em que as actividades do projecto
são representadas por arcos do grafo, e a outra, em que são representadas por
nós. Vamos usar a segunda representação.

Considere um projecto com as actividades e as relações de precedência a seguir
indicadas:

| Actividade | Duração | Precedências |
|------------|---------|--------------|
| 0          | 4       | –            |
| 1          | 6       | 0            |
| 2          | 7       | 1,4          |
| 3          | 2       | 2,5          |
| 4          | 9       | 0,7          |
| 5          | 4       | 4,8          |
| 6          | 5       | –            |
| 7          | 6       | 6            |
| 8          | 4       | 7,10         |
| 9          | 2       | 8,11         |
| 10         | 8       | 6            |
| 11         | 7       | 10           |

O grafo associado a este projecto é:

![](figures/graph.png)

No problema em análise, o caminho crítico corresponde às actividades 6, 7, 4, 2
e 3, com uma duração de 29 unidades de tempo, que é também o menor tempo
necessário para completar a execução de todo o projecto.

## Objectivo

Os trabalhos práticos experimentais visam desenvolver a capacidade de analisar
sistemas complexos, de criar modelos para os descrever, de obter soluções para
esses modelos utilizando programas computacionais adequados, de validar os
modelos obtidos, de interpretar as soluções obtidas, e de elaborar recomendações
para o sistema em análise.

## Determinação da lista de actividades

Seja _ABCDE_ o número de inscrição do aluno do grupo com maior número de
inscrição. Remova da lista de actividades as actividades _D_ e _E_, passando as
precedências a ser estabelecidas da seguinte forma:

- os sucessores da actividade _D_ passam a ter como novas precedências os
  antecessores da actividade _D_;

- o mesmo para _E_.

A título ilustrativo, se a actividade 4 for removida, a actividade 2 passa a ter
como precedências, como novas precedências, as actividades 0 e 7 (em vez da
actividade 4), o mesmo acontecendo com a actividade 5.

